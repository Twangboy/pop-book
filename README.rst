====================================
Intro to Plugin Oriented Programming
====================================

This book in intended to give people a theoretical view of Plugin Oriented
Programming. The philosophy, reason, and rules, that will lead them to writing
good Plugin Oriented Programming code.

This is not intended as a feature by feature introduction to the pop library.
This documentation will be found in the pop documentation repository.
